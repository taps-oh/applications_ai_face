import screen from '@ohos.screen';
import display from '@ohos.display';
import window from '@ohos.window';
import { initInfo } from "../utils/log";

let log = initInfo('utils');

let timer = new Map();
export function logTime(str){
    let t1 = new Date().getTime();
    if(timer.has(str)){
        let t0 = timer.get(str);
        log(`${str} end: ${t1}`);
        log(`${str} run time: ${t1 - t0} ms`);
        timer.delete(str);
    } else {
        timer.set(str, t1);
        log(`${str} start: ${t1}`);
    }
}


// 监听横竖屏
export function onScreenOrientationChange(callback) {
    screen.on("change", async (n) => {

        let screenArr = await screen.getAllScreens();
        let orientation = screenArr[0].orientation;
        //        UNSPECIFIED = 0,
        //        VERTICAL = 1,
        //        HORIZONTAL = 2,
        //        REVERSE_VERTICAL = 3,
        //        REVERSE_HORIZONTAL = 4,
        log('onScreenOrientationChange change orientation:' + orientation);
        callback(orientation)
    })
}

// 获取横竖屏
export async function getScreenOrientation() {
    let screenArr = await screen.getAllScreens();
    let orientation = screenArr[0].orientation;
    log('getScreenOrientation change orientation:' + orientation);
    return orientation;
}

// 设置横竖屏
export async function setScreenOrientation(orientation: number) {
    let screenArr = await screen.getAllScreens();
    if (orientation != 1) {
        await screenArr[0].setOrientation(screen.Orientation.VERTICAL);
    } else {
        await screenArr[0].setOrientation(screen.Orientation.HORIZONTAL);
    }
}

export async function setDensityDpi() {
    let screenArr = await screen.getAllScreens();
    await screenArr[0].setDensityDpi(320);
}

export async function getAppWindowSize(win){
    let width = 1280; // src/main/resources/base/profile/main_pages.json 中的designWidth
    // @ts-ignore
    let prop = await win.getProperties();
    let rect = prop.windowRect;
    let height = rect.height * width / rect.width;
    let lpi = parseFloat((rect.width / width).toFixed(3));
    let dis = await display.getDefaultDisplay();
    let dpi = parseFloat((dis.densityDPI / 160).toFixed(3));
    let res = {
        width: width,
        height: Math.floor(height),
        pageW: rect.width,
        pageH: rect.height,
        dpi: dpi,
        lpi: lpi
    };
    //    log('getAppWindowSize res:' + JSON.stringify(res));
    return res;
}

export async function getScreenSize() {
    let width = 1280;
    //    let cfg = await globalThis.abilityContext.resourceManager.getConfiguration();
    //    log('getScreenSize direction' + JSON.stringify(cfg.direction))
    //    let direction = cfg.direction;
    // 应用所在window尺寸
    // @ts-ignore
    let win = await window.getTopWindow(globalThis.abilityContext);
    // @ts-ignore
    let prop = await win.getProperties();
    let rect = prop.windowRect;
    let height = rect.height * 1280 / rect.width;

    let lpi = parseFloat((rect.width / 1280).toFixed(3));
    let dis = await display.getDefaultDisplay();
    let dpi = parseFloat((dis.densityDPI / 160).toFixed(3));
    let res = {
        width: width,
        height: Math.floor(height),
        pageW: rect.width,
        pageH: rect.height,
        dpi: dpi,
        lpi: lpi
    };
    return res;
}

export function objType(obj) {
    return Object.prototype.toString.call(obj).replace(/\[object /, '').replace(/\]/, '').toLowerCase();
}

export function getTimeStr(s: number) {
    //    log('getTimeStr s:' + s);
    let h = '';
    let m = '';
    let sec = ''
    let seconds = Math.floor(s / 1000);
    //    log('getTimeStr seconds:' + seconds);
    let hour = Math.floor(seconds / 3600);
    //    log('getTimeStr hour:' + hour);
    seconds = seconds - hour * 3600
    if (hour > 0) {
        h = `${hour}:`
    }
    let min = Math.floor(seconds / 60);
    //    log('getTimeStr min:' + min);
    seconds = seconds - min * 60;

    m = String(min).padStart(2, '0') + ':';
    sec = String(seconds).padStart(2, '0');
    return `${h}${m}${sec}`;
}


export function drawImageSize(imgW: number, imgH: number, canvasW: number, canvasH: number) {
    let cW =  canvasW;
    let cH =  canvasH;

    let canvas_s = cW / cH;
    let img_s = imgW / imgH;

    let img_w = cW;
    let img_h = cH;


    if (canvas_s > img_s) {
        img_w = parseInt((cH * img_s).toFixed(0))
    } else {
        img_h = parseInt((cW / img_s).toFixed(0))
    }

    let x = Math.floor((canvasW - img_w)/2);
    let y = Math.floor((canvasH - img_h)/2);

    return { x, y, width: img_w, height: img_h };
}


export function getImageSize(pageW: number, pageH: number, imgW: number, imgH: number, canvasW: number, canvasH: number) {
    //    let cW =  pageW > canvasW ? canvasW : pageW;
    //    let cH =  pageH > canvasH ? canvasH : pageH;
    let cW =  pageW;
    let cH =  pageH;

    let page_s = cW / cH;
    let img_s = imgW / imgH;
    let img_w = cW;
    let img_h = cH;

    if (page_s > img_s) {
        img_w = parseInt((cH * img_s).toFixed(0))
    } else {
        img_h = parseInt((cW / img_s).toFixed(0))
    }

    //    img_w =  Math.floor(img_w/1.5);
    //    img_h =  Math.floor(img_h/1.5);

    if(imgW < img_w){
        img_w = imgW;
        img_h = imgH
    }

    let x = Math.floor((pageW - img_w)/2);
    let y = Math.floor((pageH - img_h)/2);

    return { x, y, width: img_w, height: img_h };
}

export function getVideoSize(pageW: number, pageH: number, videoW: number, videoH: number, type: string | number=6) {
    let t = 6;
    let page_s = pageW / pageH;
    let video_s = videoW / videoH;

    let video_w = pageW;
    let video_h = pageH;

    switch (type) {
        case 0: { //4:3
            if (page_s > (4 / 3)) {
                video_w = parseInt((pageH * 4 / 3).toFixed(0))
            } else {
                video_h = parseInt((pageW * 3 / 4).toFixed(0))
            }
            break;
        }
        case 1: { //16:9
            if (page_s > (16 / 9)) {
                video_w = parseInt((pageH * 16 / 9).toFixed(0))
            } else {
                video_h = parseInt((pageW * 9 / 16).toFixed(0))
            }
            break;
        }
        case 6: {
            if (page_s > video_s) {
                video_w = parseInt((pageH * video_s).toFixed(0))
            } else {
                video_h = parseInt((pageW / video_s).toFixed(0))
            }
            break;
        }
    }
    return { width: video_w, height: video_h }
}

// 毫秒转日期
export function secToStr(sec) {
    let time = new Date(sec);
    let year: any = time.getFullYear();
    let month: any = time.getMonth() + 1;
    let day: any = time.getDate();
    let h: any = time.getHours();
    let m: any = time.getMinutes();
    let s: any = time.getSeconds();

    month = String(month).padStart(2, '0');
    day = String(day).padStart(2, '0');
    h = String(h).padStart(2, '0');
    m = String(m).padStart(2, '0');
    s = String(s).padStart(2, '0');
    return `${year}-${month}-${day} ${h}:${m}:${s}`;
}


export function isVideoFile(name: string) {
    if (name.includes('.')) {
        let type = name.split('.').pop().toLocaleLowerCase()
        let types = 'mp4;mpeg-ts;webm;mkv'.toLocaleLowerCase();
        return types.includes(type);
    }
    return false;
}



