import hilog from '@ohos.hilog';
const isConsole = false;
const isHilog = true;
const domain = 0xc000;
const domainStr = "0xc000";
const privateNum = 3;

export const log = (tag: string|number = "", log?: string|number)=> {
    let Tag = 'OH';
    if(tag){
        if(log) {
                isConsole?
            console.info(`${domain}/${domainStr}-C-${Tag}: ${tag} --> ${log}`):null;
                isHilog?
            hilog.info(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag} --> ${log}`, privateNum):null;
        }else{
                isConsole?
            console.info(`${domain}/${domainStr}-C-${Tag}: ${tag}`):null;
                isHilog?
            hilog.info(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag}`, privateNum):null;
        }
    }else{
        if(log) {
                isConsole?
            console.info(`${domain}/${domainStr}-C-${Tag}: ${log}`):null;
                isHilog?
            hilog.info(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${log}`, privateNum):null;
        }
    }
}

export const debug = (tag: string|number = "", log?: string|number)=> {
    let Tag = 'OH';
    if(tag){
        if(log) {
                isConsole?
            console.debug(`${domain}/${domainStr}-C-${Tag}: ${tag} --> ${log}`):null;
                isHilog?
            hilog.debug(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag} --> ${log}`, privateNum):null;
        }else{
                isConsole?
            console.debug(`${domain}/${domainStr}-C-${Tag}: ${tag}`):null;
                isHilog?
            hilog.debug(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag}`, privateNum):null;
        }
    }else{
        if(log) {
                isConsole?
            console.debug(`${domain}/${domainStr}-C-${Tag}: ${log}`):null;
                isHilog?
            hilog.debug(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${log}`, privateNum):null;
        }
    }
}

export const info = (tag: string|number = "", log?: string|number)=> {
    let Tag = 'OH';
    if(tag){
        if(log) {
                isConsole?
            console.info(`${domain}/${domainStr}-C-${Tag}: ${tag} --> ${log}`):null;
                isHilog?
            hilog.info(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag} --> ${log}`, privateNum):null;
        }else{
                isConsole?
            console.info(`${domain}/${domainStr}-C-${Tag}: ${tag}`):null;
                isHilog?
            hilog.info(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag}`, privateNum):null;
        }
    }else{
        if(log) {
                isConsole?
            console.info(`${domain}/${domainStr}-C-${Tag}: ${log}`):null;
                isHilog?
            hilog.info(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${log}`, privateNum):null;
        }
    }
}

export const err = (tag: string|number = "", log?: string|number)=> {
    let Tag = 'OH';
    if(tag){
        if(log) {
                isConsole?
            console.error(`${domain}/${domainStr}-C-${Tag}: ${tag} --> ${log}`):null;
                isHilog?
            hilog.error(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag} --> ${log}`, privateNum):null;
        }else{
                isConsole?
            console.error(`${domain}/${domainStr}-C-${Tag}: ${tag}`):null;
                isHilog?
            hilog.error(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag}`, privateNum):null;
        }
    }else{
        if(log) {
                isConsole?
            console.error(`${domain}/${domainStr}-C-${Tag}: ${log}`):null;
                isHilog?
            hilog.error(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${log}`, privateNum):null;
        }
    }
}

export const warn = (tag: string|number = "", log?: string|number)=> {
    let Tag = 'OH';
    if(tag){
        if(log) {
                isConsole?
            console.warn(`${domain}/${domainStr}-C-${Tag}: ${tag} --> ${log}`):null;
                isHilog?
            hilog.warn(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag} --> ${log}`, privateNum):null;
        }else{
                isConsole?
            console.warn(`${domain}/${domainStr}-C-${Tag}: ${tag}`):null;
                isHilog?
            hilog.warn(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag}`, privateNum):null;
        }
    }else{
        if(log) {
                isConsole?
            console.warn(`${domain}/${domainStr}-C-${Tag}: ${log}`):null;
                isHilog?
            hilog.warn(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${log}`, privateNum):null;
        }
    }
}

export const initLog = (str?:string) => {
    let Tag = str?str:'OH';
    return (tag: string|number = "", log?: string|number)=> {
        if(tag){
            if(log) {
                    isConsole?
                console.info(`${domain}/${domainStr}-C-${Tag}: ${tag} --> ${log}`):null;
                    isHilog?
                hilog.info(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag} --> ${log}`, privateNum):null;
            }else{
                    isConsole?
                console.info(`${domain}/${domainStr}-C-${Tag}: ${tag}`):null;
                    isHilog?
                hilog.info(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag}`, privateNum):null;
            }
        }else{
            if(log) {
                    isConsole?
                console.info(`${domain}/${domainStr}-C-${Tag}: ${log}`):null;
                    isHilog?
                hilog.info(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${log}`, privateNum):null;
            }
        }
    }
}

export const initDebug = (str?:string) => {
    let Tag = str?str:'OH';
    return (tag: string|number = "", log?: string|number)=> {
        if(tag){
            if(log) {
                    isConsole?
                console.debug(`${domain}/${domainStr}-C-${Tag}: ${tag} --> ${log}`):null;
                    isHilog?
                hilog.debug(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag} --> ${log}`, privateNum):null;
            }else{
                    isConsole?
                console.debug(`${domain}/${domainStr}-C-${Tag}: ${tag}`):null;
                    isHilog?
                hilog.debug(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag}`, privateNum):null;
            }
        }else{
            if(log) {
                    isConsole?
                console.debug(`${domain}/${domainStr}-C-${Tag}: ${log}`):null;
                    isHilog?
                hilog.debug(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${log}`, privateNum):null;
            }
        }
    }
}

export const initInfo = (str?:string) => {
    let Tag = str?str:'OH';
    return (tag: string|number = "", log?: string|number)=> {
        if(tag){
            if(log) {
                    isConsole?
                console.info(`${domain}/${domainStr}-C-${Tag}: ${tag} --> ${log}`):null;
                    isHilog?
                hilog.info(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag} --> ${log}`, privateNum):null;
            }else{
                    isConsole?
                console.info(`${domain}/${domainStr}-C-${Tag}: ${tag}`):null;
                    isHilog?
                hilog.info(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag}`, privateNum):null;
            }
        }else{
            if(log) {
                    isConsole?
                console.info(`${domain}/${domainStr}-C-${Tag}: ${log}`):null;
                    isHilog?
                hilog.info(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${log}`, privateNum):null;
            }
        }
    }
}

export const initErr = (str?:string) => {
    let Tag = str?str:'OH';
    return (tag: string|number = "", log?: string|number)=> {
        if(tag){
            if(log) {
                    isConsole?
                console.error(`${domain}/${domainStr}-C-${Tag}: ${tag} --> ${log}`):null;
                    isHilog?
                hilog.error(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag} --> ${log}`, privateNum):null;
            }else{
                    isConsole?
                console.error(`${domain}/${domainStr}-C-${Tag}: ${tag}`):null;
                    isHilog?
                hilog.error(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag}`, privateNum):null;
            }
        }else{
            if(log) {
                    isConsole?
                console.error(`${domain}/${domainStr}-C-${Tag}: ${log}`):null;
                    isHilog?
                hilog.error(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${log}`, privateNum):null;
            }
        }
    }
}

export const initWarn = (str?:string) => {
    let Tag = str?str:'OH';
    return (tag: string|number = "", log?: string|number)=> {
        if(tag){
            if(log) {
                    isConsole?
                console.warn(`${domain}/${domainStr}-C-${Tag}: ${tag} --> ${log}`):null;
                    isHilog?
                hilog.warn(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag} --> ${log}`, privateNum):null;
            }else{
                    isConsole?
                console.warn(`${domain}/${domainStr}-C-${Tag}: ${tag}`):null;
                    isHilog?
                hilog.warn(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${tag}`, privateNum):null;
            }
        }else{
            if(log) {
                    isConsole?
                console.warn(`${domain}/${domainStr}-C-${Tag}: ${log}`):null;
                    isHilog?
                hilog.warn(domain, `${domainStr}-H-${Tag}`, "%{public}s %{private}d", ` ${log}`, privateNum):null;
            }
        }
    }
}

export const Logger = { log, err, info, warn, debug, initLog, initErr, initInfo, initWarn, initDebug };