import hilog from '@ohos.hilog';
import Ability from '@ohos.app.ability.UIAbility';
import Window from '@ohos.window';
import display from '@ohos.display';
import deviceInfo from '@ohos.deviceInfo';
import { getAppWindowSize } from '../utils/utils'
import { userGrantPermission } from '../utils/permission';

export default class MainAbility extends Ability {
    onCreate(want, launchParam) {
        globalThis.abilityContext = this.context;
        globalThis.deviceType = 'default';
        let softwareModel = deviceInfo.softwareModel.toLowerCase();
        //        log(`softwareModel: ${softwareModel}`)
        if(softwareModel.startsWith('tv')){
            globalThis.deviceType = 'tv';
        }
    }

    onDestroy() {
        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onDestroy');
    }

    async onWindowStageCreate(windowStage: Window.WindowStage) {
        // Main window is created, set main page for this ability
        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageCreate');

        // 屏幕宽高
        globalThis.screenWidth = (await this.getWindowDisplayData()).width
        globalThis.screenHeight = (await this.getWindowDisplayData()).height
        windowStage.getMainWindow().then((win)=> {
            globalThis.mainWindow = win;
            win.setKeepScreenOn(true).then(() => {})
            getAppWindowSize(win).then((data) => {
                AppStorage.SetOrCreate<number>('screen_px_w', data.pageW)
                AppStorage.SetOrCreate<number>('screen_px_h', data.pageH)
                AppStorage.SetOrCreate<number>('screen_lpx_w', data.width)
                AppStorage.SetOrCreate<number>('screen_lpx_h', data.height)
                AppStorage.SetOrCreate<number>('screen_dpi', data.dpi)
                AppStorage.SetOrCreate<number>('screen_lpi', data.lpi)
                globalThis.lpi = data.lpi;
                globalThis.lpiPx = (num: number) => {
                    return globalThis.lpi * num + 'px';
                };

                if(globalThis.deviceType == 'default'){
                    userGrantPermission(globalThis.abilityContext, {
                        permissions: [
                            {
                                "name": "ohos.permission.MICROPHONE"
                            },
                            {
                                "name": "ohos.permission.CAMERA"
                            },
                            {
                                "name": "ohos.permission.READ_MEDIA"
                            },
                            {
                                "name": "ohos.permission.WRITE_MEDIA"
                            },
                            {
                                "name": "ohos.permission.MEDIA_LOCATION"
                            },
                            {
                                "name": "ohos.permission.READ_IMAGEVIDEO"
                            },
                            {
                                "name": "ohos.permission.READ_AUDIO"
                            },
                            {
                                "name": "ohos.permission.READ_DOCUMENT"
                            }
                        ]
                    })
                }
                windowStage.loadContent('pages/index', (err, data) => {
                    if (err.code) {
                        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.ERROR);
                        hilog.error(0x0000, 'testTag', 'Failed to load the content. Cause: %{public}s', JSON.stringify(err) ?? '');
                        return;
                    }
                    hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
                    hilog.info(0x0000, 'testTag', 'Succeeded in loading the content. Data: %{public}s', JSON.stringify(data) ?? '');
                });
            })
        })
    }

    onWindowStageDestroy() {
        // Main window is destroyed, release UI related resources
        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onWindowStageDestroy');
    }

    onForeground() {
        // Ability has brought to foreground
        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onForeground');
    }

    onBackground() {
        // Ability has back to background
        hilog.isLoggable(0x0000, 'testTag', hilog.LogLevel.INFO);
        hilog.info(0x0000, 'testTag', '%{public}s', 'Ability onBackground');
    }

    private async getWindowDisplayData() {
        let displayData: display.Display = null;
        await display.getDefaultDisplay().then((res) => {
            displayData = res;
        }).catch((err) => {
            console.info(`getWindowDisplayData error: ${err}`);
        });
        return displayData;
    }
}
