/* AI人脸识别主页 */
import router from '@ohos.router';
import { KeyCode } from '@ohos.multimodalInput.keyCode';
import { getScreenSize, drawImageSize, logTime } from '../utils/utils';
import { initInfo } from '../utils/log';
import { CRect2f, _ratio_enum, _scale_enum, bbox_pred, nms_cpu } from '../utils/caffe';
import image from '@ohos.multimedia.image';
import prompt from '@ohos.prompt';

// @ts-ignore
import mslite from '@ohos.ai.mslite';

let log = initInfo('index.ets');

@Entry
@Component
struct Index {
  private settings: RenderingContextSettings = new RenderingContextSettings(true);
  private canvasContext: CanvasRenderingContext2D = new CanvasRenderingContext2D(this.settings);

  @State canvasW: number = 720;
  @State canvasH: number = 480;

  imgBuffer: any;
  inputArray: number[];

  @StorageLink('imageUri') imageUri: string = '';
  @StorageLink('image_fd') image_fd: number = 0;
  @StorageLink('fileAsset') fileAsset: string = "";
  @StorageLink('shouldScan') shouldScan: boolean = false;
  @State isScanning: boolean = false;
  @State focusId: string|number = 1;
  @State screenW: number = 1920;
  @State screenH: number = 1080;

  @State pageW: number = 1920;
  @State pageH: number = 1080;
  @State orientation: number = 1;

  image:any;

  @State positionX: number = 150;
  @State positionY: number = 150;

  @State scannerShadowOffsetY: number = -10;

  @State faces: Array<any> = [
    //    {
    //      finalbox:{x:200,y:200,width:40,height:40, size:10},
    //      pts:[{x: 210, y:210},{x: 230, y:210},{x: 220, y:220},{x: 210, y:230},{x: 230, y:230}]
    //    }
  ]

  @State offsetX:number = 0;
  @State offsetY:number = 0;

  @State updatePage: boolean = false;

  aboutToAppear() {
    // @ts-ignore
    setAppBgColor('#182431');
    log('aboutToAppear');

    getScreenSize().then((data)=>{
      AppStorage.SetOrCreate<number>('screen_px_w',data.pageW)
      AppStorage.SetOrCreate<number>('screen_px_h',data.pageH)
      AppStorage.SetOrCreate<number>('screen_lpx_w',data.width)
      AppStorage.SetOrCreate<number>('screen_lpx_h',data.height)
      AppStorage.SetOrCreate<number>('screen_dpi',data.dpi)
      AppStorage.SetOrCreate<number>('screen_lpi',data.lpi)
      this.screenW = data.width;
      this.screenH = data.height;
      this.pageW = data.pageW;
      this.pageH = data.pageH;
      this.offsetX = Math.floor((this.pageW - this.canvasW)/2);
      this.offsetY = Math.floor((this.pageH - this.canvasH)/2);
    });
  }

  onPageShow(){
    log('index onPageShow');
    log('index onPageShow this.shouldScan:' + this.shouldScan);

//    this.updatePage = true;
//    setTimeout(()=>{
//      this.updatePage = false;
//    }, 0)

    if(this.shouldScan){
      this.faces = [];
      this.shouldScan = false;
      this.image = null;
      this.loadImg();
    }
  }

//  onBackPress(){
//    router.replaceUrl({ url: "pages/home" });
//    router.clear();
//    return true;
//  }

  // 选在图片后加载
  loadImg(){
    this.canvasContext.clearRect(0, 0, this.canvasW, this.canvasH);
    let imageSourceApi = image.createImageSource(this.image_fd);
    imageSourceApi.createPixelMap(async (err, data)=>{
      if(data){
        this.image = data;
        data.getImageInfo().then((image)=>{
          let width = image.size.width;
          let height = image.size.height;
          let imgSize = drawImageSize(width, height, this.canvasW, this.canvasH);
          log('loadImg imgSize:' + JSON.stringify(imgSize));
          this.canvasContext.drawImage(data, imgSize.x, imgSize.y, imgSize.width, imgSize.height);

          this.isScanning = true;
          setTimeout(()=>{
            let rgba = this.canvasContext.getImageData(0, 0, this.canvasW, this.canvasH).data;
            let size = this.canvasW * this.canvasH;

            let bgr = new Float32Array(size * 3);
            for(let i=0; i<size; i++){
              bgr[i*3 + 0] = rgba[i*4 + 2];
              bgr[i*3 + 1] = rgba[i*4 + 1];
              bgr[i*3 + 2] = rgba[i*4 + 0];
            }
            this.imgBuffer = bgr.buffer;
            this.imageScan();
          },100)
        });
      }else{
        this.isScanning = false;
        prompt.showToast({
          message: "图片加载失败，请重新选择图片。",
          duration: 3000,
          bottom: `${px2vp(this.pageH+100)/2}`
        })
        log(`loadImg createPixelMap err:` + err.code );
      }
    })
  }
  // 绘制人脸
  drawFace(data){
    if(data.length < 1){
      prompt.showToast({
        message: "未识别到人脸，请重试。",
        duration: 3000,
        bottom: `${px2vp(this.pageH+100)/2}`
      })
    } else {
      let faces:any = [];
      data.forEach((item)=>{
        let finalbox = item.finalbox;
        //      let score = item.score;
        //      let pts = item.pts;

        let x = finalbox.x + this.offsetX;
        let y = finalbox.y + this.offsetY;

        let w = finalbox.w - finalbox.x;
        let h = finalbox.h - finalbox.y;

        let size1 = Math.floor(w/5);
        let size2 = Math.floor(h/5);
        let size = size1 > size2 ? size2 : size1;
        let face = {
          finalbox:{ x,y,size,width:w,height:h },
          pts:[]
        };
        faces.push(face);
      });
      this.faces = faces;
    }
    this.isScanning = false;
  }

  // 数据后处理
  acFilterAnchor(acArray){
    let anchor_num = 2;
    let stride = [32, 16, 8];
    let anchor_cfg = [
      { "SCALES": [32, 16], RATIOS: [1], "BASE_SIZE": 16 },
      { "SCALES": [8, 4], RATIOS: [1], "BASE_SIZE": 16 },
      { "SCALES": [2, 1], RATIOS: [1], "BASE_SIZE": 16 }
    ];

    let confidence_threshold = 0.5
    let nms_threshold = 0.4;

    let ratiow = 1.0;
    let ratioh = 1.0;

    let proposals = [];

    acArray.forEach((ac, index) => {
      let clsData = new Float32Array(ac['cls'].data());
      let regData = new Float32Array(ac['reg'].data());
      //      let ptsData = new Float32Array(ac['pts'].data());

      //      log(`clsData length: ${clsData.length}`);
      //      log(`regData length: ${regData.length}`);
      //      log(`ptsData length: ${ptsData.length}`);

      let pts_length = 0;
      //      if (ptsData) {
      //        pts_length = ac['pts'].shape[1]/anchor_num/2;
      //      }

      let w = ac['cls'].shape[3];
      let h = ac['cls'].shape[2];
      let step = h*w;

      let anchor_stride = stride[index];
      let cfg = anchor_cfg[index];

      let base_anchor = new CRect2f(0, 0, cfg["BASE_SIZE"]-1, cfg["BASE_SIZE"]-1);
      let ratio_anchors = _ratio_enum(base_anchor.val, cfg["RATIOS"])
      let preset_anchors = _scale_enum(ratio_anchors, cfg["SCALES"]);

      for (let i = 0; i < w; i++) {
        for (let j = 0; j < h; j++) {
          let id = j*w+i;
          for (let a = 0; a < anchor_num; a++){
            let cls_score = clsData[(anchor_num + a)*step + id];
            if(cls_score > confidence_threshold) {
              let boxObj = new CRect2f(i * anchor_stride + preset_anchors[a][0],
                j * anchor_stride + preset_anchors[a][1],
                i * anchor_stride + preset_anchors[a][2],
                j * anchor_stride + preset_anchors[a][3]);
              let box = boxObj.val;

              let deltaObj = new CRect2f(regData[(a * 4 + 0) * step+id], regData[(a * 4 + 1) * step+id], regData[(a * 4 + 2) * step+id], regData[(a * 4 + 3) * step+id]);
              let delta = deltaObj.val;
              let res = {
                score: 0,
                finalbox: {},
                pts: []
              };

              res.finalbox = bbox_pred(box, delta);
              res.score = cls_score;
              //              if (ptsData) {
              //                let pts_delta = [];
              //                for (let p = 0; p < pts_length; p++) {
              //                  pts_delta[p] = {
              //                    x: ptsData[(a * 10 + p * 2) * step+id],
              //                    y: ptsData[(a * 10 + p * 2 + 1) * step+id]
              //                  }
              //                }
              //                res.pts = landmark_pred(box, pts_delta);
              //              }
              //              log(`proposals data: ${JSON.stringify(res)}`);
              proposals.push(res);
            }
          }
        }
      }
    })

    let data = nms_cpu(proposals, nms_threshold);
    log('nms_cpu data:' + JSON.stringify(data));
    this.drawFace(data);
  }

  // AI接口推理图片
  async imageScan() {
    let modelName = "mnet_caffemodel_480_720.ms";
    let option = {
      target: ["cpu"]
    };

    //      let modelName = "mnet.ms";
    //      let option = {
    //        target: ["nnrt"]
    //      };

    globalThis.abilityContext.resourceManager.getRawFile(modelName).then(async (modelData)=>{
      try{
        let modelBuffer = modelData.buffer; // 模型文件数据 ArrayBufferLike

        let msliteModel = await mslite.loadModel(modelBuffer, option); // 加载模型
        log(`loadModel`);
        let modelInputs = msliteModel.getInputs(); // 获取模型输入 返回数组类型
        log(`getInputs`);
        if(modelInputs){
          log(`modelInputs length:` + modelInputs.length);
        }else{
          log(`modelInputs:` + JSON.stringify(modelInputs));
        }

        modelInputs.forEach((item, index)=>{
          item.setData(this.imgBuffer); // 将图片输入
        })

        let modelOutputs = await msliteModel.predict(modelInputs); // 执行推理
        log(`predict`);
        if(modelInputs){
          log(`modelOutputs length:` + modelOutputs.length);
        }else{
          log(`modelOutputs:` + JSON.stringify(modelOutputs));
        }
        let acArray = [{},{},{}]; // {32, 16, 8}
        modelOutputs.forEach((item, index)=>{
          let name = item.name;
          let key = "";
          if(name.includes('face_rpn_cls_prob_reshape_stride')){
            key = "cls";
          } else if(name.includes('face_rpn_bbox_pred_stride')){
            key = "reg";
          }
          //            else if(name.includes('face_rpn_landmark_pred_stride')){
          //              key = "pts";
          //            }
          if(name.includes('32')){
            acArray[0][key] = item;
          }else if(name.includes('16')){
            acArray[1][key] = item;
          }else if(name.includes('8')){
            acArray[2][key] = item;
          }
        })
        this.acFilterAnchor(acArray);
      }catch(e){
        this.isScanning = false;
        log(`imageScan try catch e:` + e);
      }
    }).catch((e)=>{
      this.isScanning = false;
      log(`modelData err:` + e );
      prompt.showToast({
        message: "AI模型加载失败，请重试！",
        duration: 3000,
        bottom: `${px2vp(this.pageH+100)/2}`
      })
    })

  }

  openImageSelect() {
    router.pushUrl({
      url: 'pages/imageSelect',
      params: {
        "from": 1
      }
    });
  }

  build() {
    Stack({ alignContent: Alignment.Center }) {
      // 图片加载
      Canvas(this.canvasContext)
        .width(`${this.canvasW}px`)
        .height(`${this.canvasH}px`)
        .backgroundColor('#00000000')
        .zIndex(2)
        .onReady(()=>{

        })
      //      Row(){
      //        Image(this.image)
      //          .width(`${this.canvasW}px`)
      //          .height(`${this.canvasH}px`)
      //          .objectFit(ImageFit.Contain)
      //      }
      //      .width(`${this.canvasW}px`)
      //      .height(`${this.canvasH}px`)
      //      .backgroundColor('#00000000')
      //      .zIndex(3)
      //      .visibility(this.image ? Visibility.Visible : Visibility.Hidden)

      // 人脸绘制
      ForEach(this.faces, (item, index)=>{
        Column() {
          Image($r('app.media.ic_left_above'))
            .width(`${item.finalbox.size}px`)
            .height(`${item.finalbox.size}px`)
            .position({ x: `${item.finalbox.x}px`, y: `${item.finalbox.y}px` })
          Image($r('app.media.ic_right_above'))
            .width(`${item.finalbox.size}px`)
            .height(`${item.finalbox.size}px`)
            .position({ x: `${item.finalbox.x + item.finalbox.width - item.finalbox.size}px`, y: `${item.finalbox.y}px` })
          Image($r('app.media.ic_left_below'))
            .width(`${item.finalbox.size}px`)
            .height(`${item.finalbox.size}px`)
            .position({ x: `${item.finalbox.x}px`, y: `${item.finalbox.y + item.finalbox.height - item.finalbox.size}px` })
          Image($r('app.media.ic_right_below'))
            .width(`${item.finalbox.size}px`)
            .height(`${item.finalbox.size}px`)
            .position({ x: `${item.finalbox.x + item.finalbox.width - item.finalbox.size}px`, y: `${item.finalbox.y + item.finalbox.height - item.finalbox.size}px` })
        }
        .width(`${this.pageW}px`)
        .height(`${this.pageH}px`)
        .zIndex(5)
      })

      // 扫描中
      Row(){
        LoadingProgress().color("#ffffff").width('120lpx')
      }
      .backgroundColor("#73000000")
      .alignItems(VerticalAlign.Center)
      .justifyContent(FlexAlign.Center)
      .width(`${this.screenW}lpx`)
      .height(`${this.screenH}lpx`)
      .zIndex(10)
      .visibility(this.isScanning ? Visibility.Visible : Visibility.Hidden)

      Row(){
        Column() {
          Row(){
            Image(this.focusId == 1 ? $rawfile('ic_gallery_on.svg') : $rawfile('ic_gallery_off.svg'))
              .width('85lpx')
              .height('85lpx')
              .borderRadius(globalThis.lpiPx(100))
              .clip(true)
              .focusable(true)
              .focusOnTouch(true)
              .defaultFocus(true)
              .onKeyEvent((event: KeyEvent) => {
                if (event.keyCode == KeyCode.KEYCODE_ENTER && event.type == KeyType.Up) {
                  if(!this.isScanning){
                    this.openImageSelect();
                  }
                }
              })
          }
          .width('100lpx')
          .height('100lpx')
          .alignItems(VerticalAlign.Center)
          .justifyContent(FlexAlign.Center)
          .borderWidth(globalThis.lpiPx(4))
          .borderRadius(globalThis.lpiPx(100))
          .borderColor('#ffffffff')
          .backgroundColor('#0dffffff')

          Row(){
            Text("请选择一张图片")
              .fontSize('14lpx')
              .fontColor('#ffd9d9d9')
          }
          .borderRadius(globalThis.lpiPx(100))
          .backgroundColor('#32000000')
          .padding({top:'5lpx',left:'15lpx',right:'15lpx',bottom:'5lpx'})
          .margin({top:'10lpx'})
        }
        .width('150lpx')
        .onTouch((e) => {
          if (e.type == TouchType.Up && !this.isScanning) {
            this.openImageSelect();
          }
        })
      }
      .zIndex(5)
      .position({
        x:'0lxp',
        y:`${this.screenH - 150}lpx`
      })
      .width('100%')
      .alignItems(VerticalAlign.Center)
      .justifyContent(FlexAlign.Center)
    }
    .width('100%')
    .height('100%')
    .backgroundColor('#182431')
  }
}